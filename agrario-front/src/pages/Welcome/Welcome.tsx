import React from 'react'
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';


const Welcome = () => {
    return (
        <div className='welcome'>
            <span className='textHome'></span>
            <Typography mb={2} variant="body1" gutterBottom>Bienvenido a Agrar io</Typography>
            <Button variant="outlined" href='/home'>Ir a la home</Button>
        </div>
    )
}

export default Welcome